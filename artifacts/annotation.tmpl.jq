{
    ($input_file): {
        "org.opencontainers.image.title": $output_file,
        "org.opencontainers.image.created": $date,
        "org.opencontainers.image.description": $file_description
    },
    "$config": {
        "org.opencontainers.image.created": $date
    },
    "$manifest": {
        "org.opencontainers.image.title": "Simple Load Balancer",
        "org.opencontainers.image.description": "Simple Load Balancer for... uh... load balancing, right?",
        "org.opencontainers.image.vendor": "rdnxk",
        "org.opencontainers.image.authors": "rdnxk, San 'rdn' Mônico <san@monico.com.br>",
        "org.opencontainers.image.url": "https://hub.docker.com/r/redemonbr/simple-load-balancer",
        "org.opencontainers.image.source": "https://gitlab.com/rdnxk/simple-load-balancer-docker",
        "org.opencontainers.image.documentation": "https://gitlab.com/rdnxk/simple-load-balancer-docker/-/blob/master/README.md",
        "org.opencontainers.image.created": "1970-01-01T00:00:00.000Z"
    }
}
