## CONSTANTS
comma :::= ,
TEMP_DIR :::= .tmp
BUILD_CONTEXT :::= ./container
CONFIG_TYPE :::= application/vnd.rdnxk.manifest.config.v1+json
PEM_TYPE :::= application/x-pem-file
MARKDOWN_TYPE :::= text/markdown
PUBLICKEY_DESCRIPTION :::= PEM-formatted public key for validating images and artifacts built by rdnxk and ReDemoNBR
LICENSE_DESCRIPTION :::= License for this image
README_DESCRIPTION :::= Usage of this image
