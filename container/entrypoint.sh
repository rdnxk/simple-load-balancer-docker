#!/usr/bin/env sh

sh /opt/simple-load-balancer/validate.sh

sh /opt/simple-load-balancer/generate-caddyfile.sh

if [ "$SLB_VALIDATE_CADDYFILE" -eq 1 ] || [ "$(echo "$SLB_VALIDATE_CADDYFILE" | tr '[:upper:]' '[:lower:]')" = "true" ]; then
    caddy validate --config /etc/caddy/Caddyfile --adapter caddyfile
fi

exec "$@"
