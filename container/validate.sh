#!/bin/sh

if [ -z "$SLB_FROM_HOST" ]; then
    echo "SLB_FROM_HOST environment variable must be set"
    exit 1
fi

if [ -z "$SLB_TO_UPSTREAMS" ]; then
    echo "SLB_TO_UPSTREAMS environment variable must be set"
    exit 1
fi

if [ -n "$SLB_LB_POLICY" ]; then 
	if ! grep "$(echo "$SLB_LB_POLICY" | cut -d' ' -f1)" /opt/simple-load-balancer/lb-policy-values.txt ; then
    	echo "Invalid SLB_LB_POLICY value '$SLB_LB_POLICY'. Read the Caddy documentation for possible values at"
    	echo "https://caddyserver.com/docs/caddyfile/directives/reverse_proxy#load-balancing"
    	exit 1
	fi
fi
