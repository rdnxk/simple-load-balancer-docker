#!/usr/bin/env sh

config_file=/opt/simple-load-balancer/config.txt
touch "$config_file"

for upstream in $(echo "$SLB_TO_UPSTREAMS" | tr ',' '\n'); do
    echo "to $upstream" >> "$config_file"
done

## Load balancing
[ -n "$SLB_LB_POLICY" ] && echo "lb_policy $SLB_LB_POLICY" >> "$config_file"
[ -n "$SLB_LB_TRY_DURATION" ] && echo "lb_try_duration $SLB_LB_TRY_DURATION" >> "$config_file"
[ -n "$SLB_LB_TRY_INTERVAL" ] && echo "lb_try_interval $SLB_LB_TRY_INTERVAL" >> "$config_file"

## Active health checking
[ -n "$SLB_HEALTH_URI" ] && echo "health_uri $SLB_HEALTH_URI" >> "$config_file"
[ -n "$SLB_HEALTH_PORT" ] && echo "health_port $SLB_HEALTH_PORT" >> "$config_file"
[ -n "$SLB_HEALTH_INTERVAL" ] && echo "health_interval $SLB_HEALTH_INTERVAL" >> "$config_file"
[ -n "$SLB_HEALTH_STATUS" ] && echo "health_status $SLB_HEALTH_STATUS" >> "$config_file"
[ -n "$SLB_HEALTH_BODY" ] && echo "health_body $SLB_HEALTH_BODY" >> "$config_file"
if [ -n "$SLB_HEALTH_HEADERS" ]; then
    echo "health_headers {" >> "$config_file"
    for health_header in $(echo "$SLB_HEALTH_HEADERS" | tr ',' '\n'); do
        echo "$(echo "$health_header" | cut -d= -f1) $(echo "$health_header" | cut -d= -f2)" >> "$config_file"
    done
    echo "}" >> "$config_file"
fi

## Passive health checking
[ -n "$SLB_FAIL_DURATION" ] && echo "fail_duration $SLB_FAIL_DURATION" >> "$config_file"
[ -n "$SLB_MAX_FAILS" ] && echo "max_fails $SLB_MAX_FAILS" >> "$config_file"
[ -n "$SLB_UNHEALTHY_STATUS" ] && echo "unhealthy_status $SLB_UNHEALTHY_STATUS" >> "$config_file"
[ -n "$SLB_UNHEALTHY_LATENCY" ] && echo "unhealthy_latency $SLB_UNHEALTHY_LATENCY" >> "$config_file"
[ -n "$SLB_UNHEALTHY_REQUEST_COUNT" ] && echo "unhealthy_request_count $SLB_UNHEALTHY_REQUEST_COUNT" >> "$config_file"

## Streaming
[ -n "$SLB_FLUSH_INTERVAL" ] && echo "flush_interval $SLB_FLUSH_INTERVAL" >> "$config_file"
if [ "$SLB_BUFFER_REQUESTS" = "1" ] || [ "$(echo "$SLB_BUFFER_REQUESTS" | tr '[:upper:]' '[:lower:]')" = "true" ]; then
    echo "buffer_requests" >> "$config_file"
fi
if [ "$SLB_BUFFER_RESPONSES" = "1" ] || [ "$(echo "$SLB_BUFFER_RESPONSES" | tr '[:upper:]' '[:lower:]')" = "true" ]; then
    echo "buffer_responses" >> "$config_file"
fi
[ -n "$SLB_MAX_BUFFER_SIZE" ] && echo "max_buffer_size $SLB_MAX_BUFFER_SIZE" >> "$config_file"

## Header manipulation
for header_up in $(echo "$SLB_HEADER_UP" | tr ',' '\n'); do
    echo "header_up $(echo "$header_up" | cut -d= -f1) $(echo "$header_up" | cut -d= -f2)" >> "$config_file"
done
for header_down in $(echo "$SLB_HEADER_DOWN" | tr ',' '\n'); do
    echo "header_down $(echo "$header_down" | cut -d= -f1) $(echo "$header_down" | cut -d= -f2)" >> "$config_file"
done

cat <<EOF > /etc/caddy/Caddyfile
{
    admin off
}

$SLB_FROM_HOST {
    reverse_proxy {
        $(cat "$config_file")
    }
}
EOF

if [ "$SLB_FORMAT_CADDYFILE" = "1" ] || [ "$(echo "$SLB_FORMAT_CADDYFILE" | tr '[:upper:]' '[:lower:]')" = "true" ]; then
    caddy fmt --overwrite /etc/caddy/Caddyfile
fi
