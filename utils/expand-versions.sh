#!/usr/bin/env bash

set -e

version="$1"

if [[ -z "$version" ]]; then
    echo "version is required"
    exit 1
fi

major="$(echo "$version" | cut -d. -f1)"
minor="$(echo "$version" | cut -d. -f2)"
patch="$(echo "$version" | cut -d. -f3)"

output=()
for variant in "$major" "${major}.$minor" "${major}.${minor}.$patch"; do
    output+=("$variant")
done

echo "${output[*]}"
